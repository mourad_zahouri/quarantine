package com.nespresso.sofa.interview.hospital.soluce.treatment;

import com.nespresso.sofa.interview.hospital.soluce.Treatment;
import com.nespresso.sofa.interview.hospital.soluce.treatmentImpl.AntibioticTreatment;
import com.nespresso.sofa.interview.hospital.soluce.treatmentImpl.AspirinTreatment;
import com.nespresso.sofa.interview.hospital.soluce.treatmentImpl.InsulinTreatment;
import com.nespresso.sofa.interview.hospital.soluce.treatmentImpl.ParacetamolTreatment;

public class TreatmentFactory {

	public static ITreatment getTreatment(Treatment treatment) {
		switch (treatment) {
		case ANTIBIOTIC:
			return new AntibioticTreatment();
		case ASPIRIN:
			return new AspirinTreatment();
		case INSULIN:
			return new InsulinTreatment();
		case PARACETAMOL:
			return new ParacetamolTreatment();
		default:
			return null;
		}

	}
}
