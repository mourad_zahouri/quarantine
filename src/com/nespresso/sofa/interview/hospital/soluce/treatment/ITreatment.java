package com.nespresso.sofa.interview.hospital.soluce.treatment;

import com.nespresso.sofa.interview.hospital.soluce.QuarantineBean;

public interface ITreatment {
	void apply(QuarantineBean quarantine);
}
