package com.nespresso.sofa.interview.hospital.soluce;

public enum Treatment{
	WAIT40DAYS,
	ASPIRIN,
	ANTIBIOTIC,
	INSULIN,
	PARACETAMOL
}
