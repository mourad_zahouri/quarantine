package com.nespresso.sofa.interview.hospital.soluce;

import java.util.HashMap;
import java.util.Map;

public class QuarantineBeanFactory {
	public QuarantineBean buildQuarantine(String _data){
		QuarantineBean quarantine = new QuarantineBean();
		Map<HealtStatus, Integer> patients = new HashMap<HealtStatus, Integer>();
		String[] values = _data.split(",");
		Integer numberOfHealthyPatient = 0;
		Integer numberOfFeverPatient = 0;
		Integer numberOfDiabetesPatient = 0;
		Integer numberOfTuberculosePatient = 0;
		for(String value : values){
			HealtStatus status = HealtStatus.valueOf(value);
			
			switch(status){
			case H:
				numberOfHealthyPatient++;
				continue;
			case F:
				numberOfFeverPatient++;
				continue;
			case D:
				numberOfDiabetesPatient++;
				continue;
			case T:
				numberOfTuberculosePatient++;
				continue;
			case X:
				// Throw an exception
			}
		}
		patients.put(HealtStatus.D, numberOfDiabetesPatient);
		patients.put(HealtStatus.F, numberOfFeverPatient);
		patients.put(HealtStatus.H, numberOfHealthyPatient);
		patients.put(HealtStatus.T, numberOfTuberculosePatient);
		patients.put(HealtStatus.X, 0);
		
		quarantine.setPatients(patients);
		return quarantine;
	
	}
}
