package com.nespresso.sofa.interview.hospital.soluce;

import com.nespresso.sofa.interview.hospital.soluce.treatment.ITreatment;
import com.nespresso.sofa.interview.hospital.soluce.treatment.TreatmentFactory;
import com.nespresso.sofa.interview.hospital.soluce.treatmentImpl.HealthStatusCalculator;

public class Quarantine {
	
	private QuarantineBean quarantine;
	
	public Quarantine(String _value){
		QuarantineBeanFactory quarantineFactory = new QuarantineBeanFactory();
		quarantine = quarantineFactory.buildQuarantine(_value);
	}
	
	private void applyTreatment(Treatment _treatment){
		quarantine.addTreatment(_treatment);
		ITreatment treatment = TreatmentFactory.getTreatment(_treatment);
		treatment.apply(quarantine);
	}
	

	public void wait40Days(){
		quarantine.addTreatment(Treatment.WAIT40DAYS);
		HealthStatusCalculator master = new HealthStatusCalculator();
		master.cureResolveStatus40Days(quarantine);
		
	}
	
	public void aspirin(){
		applyTreatment(Treatment.ASPIRIN);
	}
	
	public void antibiotic(){
		applyTreatment(Treatment.ANTIBIOTIC);
	}
	
	public void insulin(){
		applyTreatment(Treatment.INSULIN);
	}
	
	public void paracetamol(){
		applyTreatment(Treatment.PARACETAMOL);
	}
	
	public String report() {
		return quarantine.report();
	}
}
