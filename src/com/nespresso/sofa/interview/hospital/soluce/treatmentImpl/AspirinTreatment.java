package com.nespresso.sofa.interview.hospital.soluce.treatmentImpl;

import com.nespresso.sofa.interview.hospital.soluce.QuarantineBean;
import com.nespresso.sofa.interview.hospital.soluce.Treatment;
import com.nespresso.sofa.interview.hospital.soluce.treatment.ITreatment;

public class AspirinTreatment implements ITreatment {

	@Override
	public void apply(QuarantineBean quarantine) {
		// Check if Paracetamol is simultany with Aspirin
		if (quarantine.areSimultaniusTreatment(Treatment.PARACETAMOL, Treatment.ASPIRIN)) {
			quarantine.allPatientDead();
		}
	}

}
