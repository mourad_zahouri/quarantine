package com.nespresso.sofa.interview.hospital.soluce.treatmentImpl;

import com.nespresso.sofa.interview.hospital.soluce.HealtStatus;
import com.nespresso.sofa.interview.hospital.soluce.QuarantineBean;
import com.nespresso.sofa.interview.hospital.soluce.Treatment;
import com.nespresso.sofa.interview.hospital.soluce.treatment.ITreatment;

public class AntibioticTreatment implements ITreatment {

	@Override
	public void apply(QuarantineBean quarantine) {
		// antibiotic cure Tuberculosis
        // but healthy people catch Fever if mixed with insulin.
		
		if (quarantine.areSimultaniusTreatment(Treatment.ANTIBIOTIC, Treatment.INSULIN)) {
			quarantine.changePatientStatus(HealtStatus.F, HealtStatus.H);
		}
	}

}
