package com.nespresso.sofa.interview.hospital.soluce.treatmentImpl;

import com.nespresso.sofa.interview.hospital.soluce.HealtStatus;
import com.nespresso.sofa.interview.hospital.soluce.QuarantineBean;
import com.nespresso.sofa.interview.hospital.soluce.Treatment;

public class HealthStatusCalculator {

	public void cureResolveStatus40Days(QuarantineBean quarantine) {
		if(!quarantine.hasTreatmentSinceLastWait(Treatment.INSULIN)){
			quarantine.changePatientStatus(HealtStatus.X, HealtStatus.D);
		}
		
		if(quarantine.hasTreatmentSinceLastWait(Treatment.ASPIRIN)){
			quarantine.changePatientStatus(HealtStatus.H, HealtStatus.F);
		}
		
		if(quarantine.hasTreatmentSinceLastWait(Treatment.PARACETAMOL)){
			quarantine.changePatientStatus(HealtStatus.H, HealtStatus.F);
		}
		
		
		if(quarantine.hasTreatmentSinceLastWait(Treatment.ANTIBIOTIC)){
			quarantine.changePatientStatus(HealtStatus.H, HealtStatus.T);
		}

	}
	
}
