package com.nespresso.sofa.interview.hospital.soluce.treatmentImpl;

import com.nespresso.sofa.interview.hospital.soluce.HealtStatus;
import com.nespresso.sofa.interview.hospital.soluce.QuarantineBean;
import com.nespresso.sofa.interview.hospital.soluce.Treatment;
import com.nespresso.sofa.interview.hospital.soluce.treatment.ITreatment;

public class InsulinTreatment implements ITreatment {

	@Override
	public void apply(QuarantineBean quarantine) {
		if (quarantine.areSimultaniusTreatment(Treatment.INSULIN, Treatment.ANTIBIOTIC)) {
			quarantine.changePatientStatus(HealtStatus.F, HealtStatus.H);
		}
	}

}
