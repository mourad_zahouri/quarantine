package com.nespresso.sofa.interview.hospital.soluce;

public enum HealtStatus{
	H, // Healthy
    F, // Fever
    D, // Diabetes
    T,  //Tuberculosis
    X // dead
}