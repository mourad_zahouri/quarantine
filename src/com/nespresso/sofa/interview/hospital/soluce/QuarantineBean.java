package com.nespresso.sofa.interview.hospital.soluce;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuarantineBean {
	
	private Map<HealtStatus, Integer> patients;
	
	private List<Treatment> appliedTreatment = new ArrayList<Treatment>();

	public List<Treatment> getAppliedTreatment() {
		return appliedTreatment;
	}

	public void setAppliedTreatment(List<Treatment> appliedTreatment) {
		this.appliedTreatment = appliedTreatment;
	}

	public QuarantineBean(){
		
	}

	public Map<HealtStatus, Integer> getPatients() {
		return patients;
	}

	public void setPatients(Map<HealtStatus, Integer> patients) {
		this.patients = patients;
	}
	
	public String report() {
		String report = ""; //F:3 H:1 D:3 T:0 X:0
		report+= HealtStatus.F.toString()+":"+patients.get(HealtStatus.F) + " ";
		report+= HealtStatus.H.toString()+":"+patients.get(HealtStatus.H) + " ";
		report+= HealtStatus.D.toString()+":"+patients.get(HealtStatus.D) + " ";
		report+= HealtStatus.T.toString()+":"+patients.get(HealtStatus.T) + " ";
		report+= HealtStatus.X.toString()+":"+patients.get(HealtStatus.X);
		return report;
	}
	
	public void addTreatment(Treatment treatment){
		this.appliedTreatment.add(treatment);
	}
	
	public void changePatientStatus(HealtStatus _newStatus, HealtStatus _previosStatus){
		this.patients.put(_newStatus, this.patients.get(_newStatus) + this.patients.get(_previosStatus));
		this.patients.put(_previosStatus, 0);
	}
	
	public void allPatientDead(){
		changePatientStatus(HealtStatus.X, HealtStatus.D);
		changePatientStatus(HealtStatus.X, HealtStatus.F);
		changePatientStatus(HealtStatus.X, HealtStatus.T);
		changePatientStatus(HealtStatus.X, HealtStatus.H);
	}
	
	public int getLastWait40DaysIndex(){
		int lastIndex = -1;
		for(int i = this.appliedTreatment.size() - 1 ; i == 0 ; i--){
			if(this.appliedTreatment.get(i).equals(Treatment.WAIT40DAYS)){
				lastIndex = i;
				break;
			}
		}
		return lastIndex;
	}
	
	public int getPreviousLastWait40DaysIndex() {
		int lastIndex = -1;
		if (this.appliedTreatment.size() > 1) {
			for (int i = this.appliedTreatment.size() - 2; i == 0; i--) {
				if (this.appliedTreatment.get(i).equals(Treatment.WAIT40DAYS)) {
					lastIndex = i;
					break;
				}
			}
		}
		return lastIndex;
	}
	
	public boolean hasTreatmentSinceLastWait(Treatment treatment){
		int previosWait40Days = getPreviousLastWait40DaysIndex();
		Treatment lastEvent = null;
		for(int i = this.appliedTreatment.size() - 1 ; i > previosWait40Days ; i--){
			lastEvent = this.appliedTreatment.get(i);
			if(lastEvent.equals(treatment)){
				return true;
			}
		}
		return false;
	}
	
	public boolean areSimultaniusTreatment(Treatment treatment1, Treatment treatment2){
			return hasTreatmentSinceLastWait(treatment1) && hasTreatmentSinceLastWait(treatment2);
	}
}
